import Vue from 'vue'
import VueRouter from 'vue-router'
import * as firebase from 'firebase';

import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    firebase.auth().onAuthStateChanged((user) => {
      if(to.matched.some(record => record.meta.requiresGuest)) {
        if(user) {
          next({path:'/', query: to.fullPath})
        } else {
          next()
        }
      } 

      if(to.matched.some(record => record.meta.requiresAuth)) {
        if(!user) {
          next({path:'/auth', query: to.fullPath})
        } else {
          next()
        }
      } 
      })
    })

export default Router