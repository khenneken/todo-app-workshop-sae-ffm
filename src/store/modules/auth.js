import * as firebase from 'firebase';
import { Notify } from 'quasar';

export const state = {

}

export const actions = {
  register({ }, payload) {
    console.log(payload)
    firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password).then(function (user) {
      Notify.create("Erfolgreich registriert!")
    })
  },

  logout({ }) {
    firebase.auth().signOut().then(function (user) {
      Notify.create("Erfolgreich ausgeloggt!")
    })
  }
}

export default {
  namespaced: true,
  actions,
  state
}